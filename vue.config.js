module.exports = {
    pluginOptions: {
        electronBuilder: {
            nodeIntegration: true,
            contextIsolation: false,
        }
    },
    chainWebpack: config => {
        config.module.rule('node')
            .test(/\.node$/)
            .use('file-loader')
            .loader(require.resolve('file-loader'))
            .options({ name: 'static/bin/[name].[hash:8].[ext]' })
    }
}