# Admoot - 基于SSH的Linux系统管理工具

Admoot 是一个基于ssh的linux系统管理工具， 支持系统当前状态查看， 磁盘使用量分析，进程管理，防火墙管理，网站管理，数据库管理，定时任务管理，以及一个方便的文件管理功能，能够在线编辑文件， 以及上传或者下载文件。Admoot 不需要安装到服务器上，在本地安装并使用ssh连接到服务器，同时 admoot 不要求服务器必须为未进行任何操作的新服务器，通过指定配置，即可用来管理现有服务器。



![截图](./doc/screenshot.png)



## 操作系统支持

- [x] Ubuntu 20.04 LTS 



## 开发环境

开发依赖 :  nodejs, electron, vue, element-ui

快捷命令：

```bash
# 初始化
$ npm install

# 启动本地开发环境
$ npm run electron:serve

# 编译打包
$ npm run build
```

