class SSHClientCommander {
    /** */
    constructor ( client ) {
        this.client = client;
    }

    /** */
    async cat( file ) {
        let output = await this.client.exec(`cat ${file}`);
        return output;
    }

    /** CMD : ls */
    async ls( path ) {
        let output = await this.client.exec(`ls -al --time-style '+%Y-%m-%d %H:%M:%S' --color=never ${path}`);
        output = output.trim().split('\r\n');
        output.shift(); output.shift(); output.shift();

        let files = [];
        let lsRegex = /^(?<type>.)\S+\s+\d+\s+(?<owner>\S+)\s+(?<group>\S+)\s+(?<size>\d+)\s+(?<time>.*?:\d+)\s+(?<name>.*?)$/;
        for ( let i=0; i<output.length; i++ ){
            let lsMatch = output[i].match(lsRegex);
            if ( null == lsMatch ) {
                debugger;
            }
            let file = lsMatch.groups;
            if ( 'l' == file.type ) {
                let name = file.name.split(' -> ');
                file.name = name[0];
                file.linkTarget = name[1];
            }
            files.push(file);
        }
        return files;
    }
}

export default SSHClientCommander;