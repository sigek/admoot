class SSHSftpClient {
    /**
     * @param {*} server 
     */
    async constructor ( server ) {
        this.server = server;
        let client = server.getSshClient();
        this.sftp = await client.getSftp();
    }

    /**
     * 读取文件所有内容
     * @param {string} path
     * @returns {string} 
     */
    async fileReadAll( path ) {

    }
}

export default SSHSftpClient;