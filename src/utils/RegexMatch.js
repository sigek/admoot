class RegexMatch {
    /** 正则列表 */
    static regexMap = {
        'ps-aux' : /^(?<user>\S+)\s+(?<pid>\d+)\s+(?<cpu>[\d.]+)\s+(?<mem>[\d.]+)\s+(?<vsz>[\d.]+)\s+(?<rss>[\d.]+)\s+(?<tty>\S+)\s+(?<stat>\S+)\s+(?<start>\S+)\s+(?<time>\S+)\s+(?<command>.*)/,
        'proc-net-dev' : /(?<face>\S+):\s+(?<recBytes>\d+)\s+(?<recPackets>\d+)\s+(?<recErrs>\d+)\s+(?<recDrop>\d+)\s+(?<recFifo>\d+)\s+(?<recFrame>\d+)\s+(?<recCompressed>\d+)\s+(?<recMuticast>\d+)\s+(?<traBytes>\d+)\s+(?<traPackets>\d+)\s+(?<traErrs>\d+)\s+(?<traDrop>\d+)\s+(?<traFifo>\d+)\s+(?<traCools>\d+)\s+(?<traCarrier>\d+)\s+(?<traCompressed>\d+)/,
        'netstat-nltup' : /^(?<proto>\S+)\s+\d+\s+\d+\s+(?<address>[\d.:]+):(?<port>\d+)\s+\S+\s+(?<status>\S+)*\s+(?<pid>\d+)\/(?<program>.*)/,
    };

    /**
     * 正则匹配并获取结果
     * @param {*} name 
     * @param {*} string 
     */
    static match ( name, string ) {
        let regex = RegexMatch.regexMap[name];
        let match = string.match(regex);
        return (null == match) ? null : match.groups;
    }
}
export default RegexMatch;