class Formatter {
    /** 格式化为文件尺寸 */
    static asFileSize( value ) {
        if ( 1 > value ) {
            value = 1;
        }
        if( null == value || value=='' || 0 == value) {
            return "0 Bytes";
        }
        value = parseFloat(value);
        let index = Math.floor(Math.log(value)/Math.log(1024));
        if ( 0 > index ) {
            index = 0;
        }

        let size = value / Math.pow(1024,index);
        size = size.toFixed(2);

        let units = ["B","KB","MB","GB","TB","PB","EB","ZB","YB"];
        return size + units[index];
    }

    // 转换为 linux 文件 mode
    static asLinuxFileMode( value ) {
        let mode = ['-','-','-','-','-','-','-','-','-'];
        let marksOn = ['r','w','x','r','w','x','r','w','x'];

        value = value * 1;
        let index = 8;
        while ( index >= 0 ) {
            if ( 1 == (value & 1) ) {
                mode[index] = marksOn[index];
            }

            value = value >> 1;
            index --;
        }

        mode.splice(3, 0, ' ');
        mode.splice(7, 0, ' ');
        return mode.join('');
    }

    // 转换为日期时间
    static asDatetime( value ) {
        let date = new Date(value * 1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        if ( month < 10 ) {
            month = '0' + month;
        }
        let day = date.getDate();
        if ( day < 10 ) {
            day = '0' + day;
        }
        return `${year}-${month}-${day} ${date.toTimeString().substr(0, 8)}`;
    }
}
export default Formatter;