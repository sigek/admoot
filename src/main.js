import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element);

import VueCodeMirror from 'vue-codemirror'
import 'codemirror/lib/codemirror.css'
Vue.use(VueCodeMirror);

import Contextmenu from "vue-contextmenujs"
Vue.use(Contextmenu);

Vue.config.productionTip = false
window.app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
