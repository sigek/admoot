/**
 * 文件打开方式对照表
 */
class FileOpenHandlerMap {
    // 编辑器表
    static editorMap = {
        'text-editor' : ['cpp','hpp','md','sh'],
    };

    /**
     * 通过文件名获取打开方式名称
     * @param {*} filename 
     */
    static getHandlerName( filename ) {
        let fileExtPos = filename.indexOf('.');
        if ( -1 == fileExtPos ) {
            return null;
        }
        let fileExtName = filename.substr(fileExtPos+1);
        let handlerName = null;
        for ( let handler in FileOpenHandlerMap.editorMap ) {
            if ( -1 != FileOpenHandlerMap.editorMap[handler].indexOf(fileExtName) ) {
                handlerName = handler;
                break;
            }
        }
        return handlerName;
    }
}

export default FileOpenHandlerMap;