class CrontabParser {
    /** */
    constructor (data) {
        this.content = data;
    }

    /** @returns {Array} */
    parse() {
        let lines = this.content.split('\n');
        
        let tasks = [];
        let prevComment = null;
        for ( let i=0; i<lines.length; i++  ) {
            let line = lines[i].trim();
            if ( '' == line ) {
                continue;
            }
            
            if ( '#' == line[0] ) {
                prevComment = line;
                continue;
            }

            let task = {};
            task.defination = line;
            task.comment = null;
            if ( null != prevComment ) {
                task.comment = prevComment.substr(1).trim();
            }

            let parts = line.split(' ');
            task.minute = parts[0];
            task.hour = parts[1];
            task.date = parts[2];
            task.month = parts[3];
            task.day = parts[4];
            
            parts.splice(0, 5);
            task.cmd = parts;
            task.timeReadable = null;
            this.parseReadableTime(task);

            tasks.push(task);
            prevComment = null;
        }

        return tasks;
    }

    /** 解析时间 */
    parseReadableTime( task ) {
        if ( '*' == task.minute && '*' == task.hour && '*' == task.date && '*'  == task.month && '*' == task.day ) {
            task.timeReadable = '每分钟';
            return;
        }

        if ( '*' != task.day ) {
            this.parseReadableWeekday(task);
        } else {
            if ( '*' == task.month && '*' == task.date ) {
                task.timeReadable = [`每天`];
            } else {
                if ( '*' == task.month ) {
                    task.timeReadable = [`每月`];
                } else {
                    task.timeReadable = [`每${task.month}月`];
                }
                if ( '*' == task.date ) {
                    task.timeReadable.push('每天');
                } else {
                    task.timeReadable.push(`每${task.date}号`);
                }
            }
        }

        if ( '*' == task.minute && '*' == task.hour ) {
            task.timeReadable.push('每分钟');
        } else {
            if ( '*' == task.hour ) {
                task.timeReadable.push(`每小时`);
            } else {
                task.timeReadable.push(`${task.hour}时`);
            }
            if ( '*' == task.minute ) {
                task.timeReadable.push(`每分钟`);
            } else {
                task.timeReadable.push(`${task.minute}分`);
            }
        }

        task.timeReadable = task.timeReadable.join(' ');
    
    }

    /** 解析周信息 */
    parseReadableWeekday( task ) {
        let day = task.day.split(',');
        for ( let i=0; i<day.length; i++ ) {
            if ( -1 != day[i].indexOf('-') ) {
                day[i] = day[i].split('-');
                day[i] = `${this.parseReadableWeekdayConvertDayToNumber(day[i][0])}-${this.parseReadableWeekdayConvertDayToNumber(day[i][1])}`;
            } else if ( -1 != day[i].indexOf('/') ) {
                day[i] = day[i].split('/');
                day[i] = `${this.parseReadableWeekdayConvertDayToNumber(day[i][0])}-${this.parseReadableWeekdayConvertDayToNumber(day[i][1])}`
            } else {
                day[i] = this.parseReadableWeekdayConvertDayToNumber(day[i]);
            }
        }
        day = day.join(',');
        task.timeReadable = [`每周${day}`];
    }

    /** 将星期名称或数值转换为数值 */
    parseReadableWeekdayConvertDayToNumber( name ) {
        let nameList = ['sun','mon','tue','wed','thu','fri','sat'];
        if ( '*' == name ) {
            return '*';
        } else if ( name == parseInt(name) ) {
            return ['日','一','二','三','四','五','六'][name];
        } else if ( -1 != nameList.indexOf(name.toLowerCase()) ) {
            return this.parseReadableWeekdayConvertDayToNumber(nameList.indexOf(name.toLowerCase()));
        } else {
            return name;
        }
    }
}

export default CrontabParser;