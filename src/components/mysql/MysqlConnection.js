import mysql from 'mysql';
class MysqlConnection {
    // 构造函数
    constructor( options ) {
        this.options = options;

        this.connection = mysql.createConnection(options);
        this.connection.connect();
    }

    /** 执行SQL */
    execute( sql ) {
        let $this = this;
        return new Promise(( resolve, reject ) => {
            $this.connection.query(sql, function (error, results, fields) {
                if (error) {
                    reject(error);
                    return;
                }
                resolve(results);
            });
        });
    }

    // 断开连接
    disconnect() {
        this.connection.end();
    }

    // 转义变量
    escape( value ) {
        return this.connection.escape(value);
    }
}

export default MysqlConnection;