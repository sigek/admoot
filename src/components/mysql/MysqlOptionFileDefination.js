class MysqlOptionFileDefination {
    /**
     * 获取定义
     */
    static getDefinations() {
        let defs = {
            'common' : {
                name : '通用',
                options : {
                    "bind-address":{name:"监听地址"},
                    "port":{name:"端口号"},
                    "socket":{name:"套接字文件"},
                    "max_allowed_packet":{name:"最大接收数据包大小"},
                    "default_storage_engine":{name:"默存储引擎"},
                    "max_connections":{name:"最大连接数"},
                    "max_user_connections":{name:"最大用户连接数"},
                    "thread_cache_size":{name:"线程缓存大小"},
                    "sort_buffer_size":{name:"排序缓存大小"},
                    "tmp_table_size":{name:"临时数据表大小"},
                    "read_buffer_size":{name:"读取缓存大小"},
                    "read_rnd_buffer_size":{name:"随机读取缓存大小"},
                    "join_buffer_size":{name:"联表缓存大小"},
                    "table_definition_cache":{name:"表定义文件缓存量"},
                    "table_open_cache":{name:"表最大打开数量"},
                }
            },
            'log' : {
                name : '日志',
                options : {
                    "log_error":{name:"错误日志"},
                    "general_log_file":{name:"通用日志"},
                    "general_log":{name:"开启通用日志"},
                },
            },
            'slow-query' : {
                name : '慢查询记录',
                options : {
                    'slow_query_log' : {name:'启用慢查询日志',},
                    'slow_query_log_file' : {name:'慢查询日志文件',},
                    'log_queries_not_using_indexes' : {name:'记录未使用索引查询',},
                    'long_query_time' : {name:'慢查询耗时',},
                    'min_examined_row_limit' : {name:'慢查询最少扫描行数',},
                }
            },
            'binlog' : {
                name : '二进制日志',
                options : {
                    "log_bin":{name:"二进制日志记录方式"},
                    "binlog_cache_size":{name:"二进制日志缓存大小"},
                    "binlog_stmt_cache_size":{name:"非事务状态缓存大小"},
                    "max_binlog_size":{name:"二进制日志大小限制"},
                    "sync_binlog":{name:"二进制日志磁盘写入频率"},
                    "expire_logs_days":{name:"二进制日志文件过期天数"},
                    "binlog_format":{name:"二进制记录格式"},
                    "binlog_row_image":{name:"二进制日志行镜像模式"},
                    "log_slave_updates":{name:"二进制日志记录从服务器更新"},
                }
            },
            'others' : {
                name : '其他',
                options : {
                    "server_id":{name:"服务器ID"},
                    "read_only":{name:"从服务器只读"},
                    "skip_slave_start":{name:"从服务器跳过启动流程"},
                    "local_infile":{name:"允许LOAD DATA"},
                    "sql_mode":{name:"SQL 模式"},
                    "skip_name_resolve":{name:"禁用dns解析"},
                }
            },
            'MyISAMDB' : {
                name : 'MyISAM 引擎',
                options : {
                    'key_buffer_size':{},
                }
            },
            'MEMORYDB' : {
                name : 'Memory 引擎',
                options : {
                    'max_heap_table_size':{},
                }
            },
            'InnoDB' : {
                name : 'InnoDB 引擎',
                options : {
                    "innodb_buffer_pool_size":{name:'缓冲池大小大',},
                    "innodb_buffer_pool_chunk_size":{},
                    "innodb_buffer_pool_debug":{},
                    "innodb_buffer_pool_dump_at_shutdown":{},
                    "innodb_buffer_pool_dump_now":{},
                    "innodb_buffer_pool_dump_pct":{},
                    "innodb_buffer_pool_filename":{},
                    "innodb_buffer_pool_in_core_file":{},
                    "innodb_buffer_pool_instances":{},
                    "innodb_buffer_pool_load_abort":{},
                    "innodb_buffer_pool_load_at_startup":{},
                    "innodb_buffer_pool_load_now":{},

                    "innodb_log_file_size":{name:'Redo日志文件的大小'},
                    "innodb_log_buffer_size":{},
                    "innodb_log_checkpoint_fuzzy_now":{},
                    "innodb_log_checkpoint_now":{},
                    "innodb_log_checksums":{},
                    "innodb_log_compressed_pages":{},
                    "innodb_log_files_in_group":{},
                    "innodb_log_group_home_dir":{},
                    "innodb_log_spin_cpu_abs_lwm":{},
                    "innodb_log_spin_cpu_pct_hwm":{},
                    "innodb_log_wait_for_flush_spin_hwm":{},
                    "innodb_log_write_ahead_size":{},
                    "innodb_log_writer_threads":{},

                    "daemon_memcached_enable_binlog":{},
                    "daemon_memcached_engine_lib_name":{},
                    "daemon_memcached_engine_lib_path":{},
                    "daemon_memcached_option":{},
                    "daemon_memcached_r_batch_size":{},
                    "daemon_memcached_w_batch_size":{},
                    "innodb":{},
                    "innodb_adaptive_flushing":{},
                    "innodb_adaptive_flushing_lwm":{},
                    "innodb_adaptive_hash_index":{},
                    "innodb_adaptive_hash_index_parts":{},
                    "innodb_adaptive_max_sleep_delay":{},
                    "innodb_api_bk_commit_interval":{},
                    "innodb_api_disable_rowlock":{},
                    "innodb_api_enable_binlog":{},
                    "innodb_api_enable_mdl":{},
                    "innodb_api_trx_level":{},
                    "innodb_autoextend_increment":{},
                    "innodb_autoinc_lock_mode":{},
                    "innodb_background_drop_list_empty":{},
                    
                    "innodb_change_buffer_max_size":{},
                    "innodb_change_buffering":{},
                    "innodb_change_buffering_debug":{},
                    "innodb_checkpoint_disabled":{},
                    "innodb_checksum_algorithm":{},
                    "innodb_cmp_per_index_enabled":{},
                    "innodb_commit_concurrency":{},
                    "innodb_compress_debug":{},
                    "innodb_compression_failure_threshold_pct":{},
                    "innodb_compression_level":{},
                    "innodb_compression_pad_pct_max":{},
                    "innodb_concurrency_tickets":{},
                    "innodb_data_file_path":{},
                    "innodb_data_home_dir":{},
                    "innodb_ddl_buffer_size":{},
                    "innodb_ddl_log_crash_reset_debug":{},
                    "innodb_ddl_threads":{},
                    "innodb_deadlock_detect":{},
                    "innodb_dedicated_server":{},
                    "innodb_default_row_format":{},
                    "innodb_directories":{},
                    "innodb_disable_sort_file_cache":{},
                    "innodb_doublewrite":{},
                    "innodb_doublewrite_batch_size":{},
                    "innodb_doublewrite_dir":{},
                    "innodb_doublewrite_files":{},
                    "innodb_doublewrite_pages":{},
                    "innodb_fast_shutdown":{},
                    "innodb_fil_make_page_dirty_debug":{},
                    "innodb_file_per_table":{},
                    "innodb_fill_factor":{},
                    "innodb_flush_log_at_timeout":{},
                    "innodb_flush_log_at_trx_commit":{},
                    "innodb_flush_method":{},
                    "innodb_flush_neighbors":{},
                    "innodb_flush_sync":{},
                    "innodb_flushing_avg_loops":{},
                    "innodb_force_load_corrupted":{},
                    "innodb_force_recovery":{},
                    "innodb_fsync_threshold":{},
                    "innodb_ft_cache_size":{},
                    "innodb_ft_enable_diag_print":{},
                    "innodb_ft_enable_stopword":{},
                    "innodb_ft_max_token_size":{},
                    "innodb_ft_min_token_size":{},
                    "innodb_ft_num_word_optimize":{},
                    "innodb_ft_result_cache_limit":{},
                    "innodb_ft_server_stopword_table":{},
                    "innodb_ft_sort_pll_degree":{},
                    "innodb_ft_total_cache_size":{},
                    "innodb_ft_user_stopword_table":{},
                    "innodb_idle_flush_pct":{},
                    "innodb_io_capacity":{},
                    "innodb_io_capacity_max":{},
                    "innodb_limit_optimistic_insert_debug":{},
                    "innodb_lock_wait_timeout":{},
                    

                    
                    "innodb_lru_scan_depth":{},
                    "innodb_max_dirty_pages_pct":{},
                    "innodb_max_dirty_pages_pct_lwm":{},
                    "innodb_max_purge_lag":{},
                    "innodb_max_purge_lag_delay":{},
                    "innodb_max_undo_log_size":{},
                    "innodb_merge_threshold_set_all_debug":{},
                    "innodb_monitor_disable":{},
                    "innodb_monitor_enable":{},
                    "innodb_monitor_reset":{},
                    "innodb_monitor_reset_all":{},
                    "innodb_numa_interleave":{},
                    "innodb_old_blocks_pct":{},
                    "innodb_old_blocks_time":{},
                    "innodb_online_alter_log_max_size":{},
                    "innodb_open_files":{},
                    "innodb_optimize_fulltext_only":{},
                    "innodb_page_cleaners":{},
                    "innodb_page_size":{},
                    "innodb_parallel_read_threads":{},
                    "innodb_print_all_deadlocks":{},
                    "innodb_print_ddl_logs":{},
                    "innodb_purge_batch_size":{},
                    "innodb_purge_rseg_truncate_frequency":{},
                    "innodb_purge_threads":{},
                    "innodb_random_read_ahead":{},
                    "innodb_read_ahead_threshold":{},
                    "innodb_read_io_threads":{},
                    "innodb_read_only":{},
                    "innodb_redo_log_archive_dirs":{},
                    "innodb_redo_log_encrypt":{},
                    "innodb_replication_delay":{},
                    "innodb_rollback_on_timeout":{},
                    "innodb_rollback_segments":{},
                    "innodb_saved_page_number_debug":{},
                    "innodb_segment_reserve_factor":{},
                    "innodb_sort_buffer_size":{},
                    "innodb_spin_wait_delay":{},
                    "innodb_spin_wait_pause_multiplier":{},
                    "innodb_stats_auto_recalc":{},
                    "innodb_stats_include_delete_marked":{},
                    "innodb_stats_method":{},
                    "innodb_stats_on_metadata":{},
                    "innodb_stats_persistent":{},
                    "innodb_stats_persistent_sample_pages":{},
                    "innodb_stats_transient_sample_pages":{},
                    "innodb-status-file":{},
                    "innodb_status_output":{},
                    "innodb_status_output_locks":{},
                    "innodb_strict_mode":{},
                    "innodb_sync_array_size":{},
                    "innodb_sync_debug":{},
                    "innodb_sync_spin_loops":{},
                    "innodb_table_locks":{},
                    "innodb_temp_data_file_path":{},
                    "innodb_temp_tablespaces_dir":{},
                    "innodb_thread_concurrency":{},
                    "innodb_thread_sleep_delay":{},
                    "innodb_tmpdir":{},
                    "innodb_trx_purge_view_update_only_debug":{},
                    "innodb_trx_rseg_n_slots_debug":{},
                    "innodb_undo_directory":{},
                    "innodb_undo_log_encrypt":{},
                    "innodb_undo_log_truncate":{},
                    "innodb_undo_tablespaces":{},
                    "innodb_use_fdatasync":{},
                    "innodb_use_native_aio":{},
                    "innodb_validate_tablespace_paths":{},
                    "innodb_write_io_threads":{},
                },
            },
        };

        for ( let group in defs ) {
            for ( let option in defs[group].options ) {
                if ( undefined == defs[group].options[option].name ) {
                    defs[group].options[option].name = MysqlOptionFileDefination.optionKeyToName(option);
                }
            }
        }
        return defs;
    }

    /**
     * @param {*} str 
     * @returns 
     */
    static optionKeyToName(str){
        return str.split('_').map((item) => { 
            return item[0].toUpperCase()+item.substr(1);
        }).join(' ');
    }
}

export default MysqlOptionFileDefination;