export default {
    'account' : {
        label:'account',
        params:[
            {name:'--aaddr',label:'Aaddr',type:'text'},
            {name:'--aname',label:'Aname',type:'text'},
            {name:'--ashort',label:'Ashort',type:'boolean'},
        ]
    },
    'addrtype' : {label:'addrtype',params:[
        {name:'--src-type',label:'Src Type',type:'text'},
        {name:'--dst-type',label:'Dst Type',type:'text'},
    ]},
    'ah' : {label:'ah',params:[
        {name:'--ahspi',label:'Ahspi',type:'text'},
    ]},
    'childlevel' : {label:'childlevel',params:[
        {name:'--childlevel',label:'childlevel',type:'text'},
    ]},
    'comment' : {
        label:'comment',
        params:[
            {name:'--comment',label:'Comment',type:'text'},
        ]
    },
    'condition' : {label:'condition',params:[
        {name:'--condition',label:'Condition',type:'text'},
    ]},
    'connbytes' : {label:'connbytes',params:[
        {name:'--connbytes',label:'Connbytes',type:'text'},
        {name:'--connbytes-dir',label:'Dir',type:'select',options:['original','reply','both']},
        {name:'--connbytes-mode',label:'Mode',type:'select',options:['packets','bytes','avgpkt']},
    ]},
    'connlimit' : {label:'connlimit',params:[
        {name:'--connlimit-above',label:'Above',type:'text'},
        {name:'--connlimit-mask',label:'Mask',type:'text'},
    ]},
    'connmark' : {label:'connmark',params:[
        {name:'--mark',label:'Mark',type:'text'},
    ]},
    'connrate' : {label:'connrate',params:[
        {name:'--connrate',label:'Connrate',type:'text'},
    ]},
    'conntrack' : {label:'conntrack',params:[
        {name:'--ctstate',label:'Ctstate',type:'select',options:['INVALID','ESTABLISHED','NEW','RELATED','SNAT','DNAT'], multiple:true},
        {name:'--ctproto',label:'Ctproto',type:'text'},
        {name:'--ctorigsrc',label:'Ctorigsrc',type:'text'},
        {name:'--ctorigdst',label:'Ctorigdst',type:'text'},
        {name:'--ctreplsrc',label:'Ctreplsrc',type:'text'},
        {name:'--ctrepldst',label:'Ctrepldst',type:'text'},
        {name:'--ctstatus',label:'Ctstatus',type:'text'},
        {name:'--ctexpire',label:'Ctexpire',type:'text'},
    ]},
    'dccp' : {label:'dccp',params:[
        {name:'--source-port',label:'Source Port',type:'text'},
        {name:'--destination-port',label:'Destination Port',type:'text'},
        {name:'--dccp-types',label:'Dccp Types',type:'select',options:['REQUEST','RESPONSE','DATA','ACK','DATAACK','CLOSEREQ','CLOSE','RESET','SYNC','SYNCACK','INVALID'], multiple:true},
        {name:'--dccp-option',label:'Dccp Option',type:'text'},
    ]},
    'dscp' : {label:'dscp',params:[
        {name:'--dscp',label:'Dscp',type:'text'},
        {name:'--dscp-class',label:'Dscp Class',type:'text'},
    ]},
    'dstlimit' : {label:'dstlimit',params:[
        {name:'--dstlimit',label:'Dstlimit',type:'text'},
        {name:'--dstlimit-mode',label:'Dstlimit Mode',type:'text'},
        {name:'--dstlimit-name',label:'Dstlimit Name',type:'text'},
        {name:'--dstlimit-burst',label:'Dstlimit Burst',type:'text'},
        {name:'--dstlimit-htable-size',label:'Dstlimit Htable Size',type:'text'},
        {name:'--dstlimit-htable-max',label:'Dstlimit Htable Max',type:'text'},
        {name:'--dstlimit-htable-gcinterval',label:'Dstlimit Htable Gcinterval',type:'text'},
        {name:'--dstlimit-htable-expire',label:'Dstlimit Htable Expire',type:'text'},
    ]},
    'ecn' : {label:'ecn',params:[
        {name:'--ecn-tcp-cwr',label:'Ecn Tcp Cwr',type:'boolean'},
        {name:'--ecn-tcp-ece',label:'Ecn Tcp Ece',type:'boolean'},
        {name:'--ecn-ip-ect',label:'Ecn Ip Ect',type:'text'},
    ]},
    'esp' : {label:'esp',params:[
        {name:'--espspi',label:'ESP SPI',type:'text'},
    ]},
    'fuzzy' : {label:'fuzzy',params:[
        {name:'--lower-limit',label:'Lower Limit',type:'text'},
        {name:'--upper-limit',label:'Upper Limit',type:'text'},
    ]},
    'hashlimit' : {label:'hashlimit',params:[
        {name:'--hashlimit',label:'Hashlimit',type:'text'},
        {name:'--hashlimit-burst',label:'Hashlimit Burst',type:'text'},
        {name:'--hashlimit-mode',label:'Hashlimit Mode',type:'select',options:['destip','destip-destport']},
        {name:'--hashlimit-name',label:'Hashlimit Name',type:'text'},
        {name:'--hashlimit-htable-size',label:'Hashlimit Htable Size',type:'text'},
        {name:'--hashlimit-htable-max',label:'Hashlimit Htable Max',type:'text'},
        {name:'--hashlimit-htable-expire',label:'Hashlimit Htable Expire',type:'text'},
        {name:'--hashlimit-htable-gcinterval',label:'Hashlimit Htable Gcinterval',type:'text'},
    ]},
    'helper' : {label:'helper',params:[
        {name:'--helper',label:'Helper',type:'text'},
    ]},
    'icmp' : {label:'icmp',params:[
        {name:'--icmp-type',label:'Icmp Type',type:'text'},
    ]},
    'iprange' : {label:'iprange',params:[
        {name:'--src-range',label:'Src Range',type:'text'},
        {name:'--dst-range',label:'Dst Range',type:'text'},
    ]},
    'ipv4options' : {label:'ipv4options',params:[
        {name:'--ssrr',label:'SSRR',type:'boolean'},
        {name:'--lsrr',label:'LSRR',type:'boolean'},
        {name:'--no-srr',label:'NO SRR',type:'boolean'},
        {name:'--rr',label:'RR',type:'boolean'},
        {name:'--ts',label:'TS',type:'boolean'},
        {name:'--ra',label:'RA',type:'boolean'},
        {name:'--any-opt',label:'ANY OPT',type:'boolean'},
    ]},
    'length' : {label:'length',params:[
        {name:'--length',label:'Length',type:'text'},
    ]},
    'limit' : {label:'limit',params:[
        {name:'--limit',label:'Limit',type:'text'},
        {name:'--limit-burst',label:'Limit Burst',type:'text'},
    ]},
    'mac' : {label:'mac',params:[
        {name:'--mac-source',label:'Mac Source',type:'text'},
    ]},
    'mark' : {label:'mark',params:[
        {name:'--mark',label:'Mark',type:'text'},
    ]},
    'mport' : {label:'mport',params:[
        {name:'--source-ports',label:'Source Ports',type:'text'},
        {name:'--destination-ports',label:'Destination Ports',type:'text'},
        {name:'--ports',label:'Ports',type:'text'},
    ]},
    'multiport' : {label:'multiport',params:[
        {name:'--source-ports',label:'Source Ports',type:'text'},
        {name:'--destination-ports',label:'Destination Ports',type:'text'},
        {name:'--ports',label:'Ports',type:'text'},
    ]},
    'nth' : {label:'nth',params:[
        {name:'--every',label:'Every',type:'text'},
        {name:'--counter',label:'Counter',type:'text'},
        {name:'--start',label:'Start',type:'text'},
        {name:'--packet',label:'Packet',type:'text'},
    ]},
    'osf' : {label:'osf',params:[
        {name:'--log',label:'Log',type:'boolean'},
        {name:'--smart',label:'Smart',type:'boolean'},
        {name:'--netlink',label:'Netlink',type:'boolean'},
        {name:'--genre',label:'Genre',type:'text'},
    ]},
    'owner' : {label:'owner',params:[
        {name:'--uid-owner',label:'UID Owner',type:'text'},
        {name:'--gid-owner',label:'GID Owner',type:'text'},
        {name:'--pid-owner',label:'PID Owner',type:'text'},
        {name:'--sid-owner',label:'SID Owner',type:'text'},
        {name:'--cmd-owner',label:'CMD Owner',type:'text'},
    ]},
    'physdev' : {label:'physdev',params:[
        {name:'--physdev-in',label:'Physdev In',type:'text'},
        {name:'--physdev-out',label:'Physdev Out',type:'text'},
        {name:'--physdev-is-in',label:'Physdev Is In',type:'boolean'},
        {name:'--physdev-is-out',label:'Physdev Is Out',type:'boolean'},
        {name:'--physdev-is-bridged',label:'Physdev Is Bridged',type:'boolean'},
    ]},
    'pkttype' : {label:'pkttype',params:[
        {name:'--pkt-type',label:'PKT Type',type:'select', options:['unicast','broadcast','multicast']},
    ]},
    'policy' : {label:'policy',params:[
        {name:'--dir',label:'Dir',type:'select', options:['in','out']},
        {name:'--pol',label:'Pol',type:'select',options:['none','ipsec']},
        {name:'--strict',label:'Strict',type:'boolean'},
        {name:'--reqid',label:'Reqid',type:'text'},
        {name:'--spi',label:'Spi',type:'text'},
        {name:'--proto',label:'Proto',type:'select',options:['ah','esp','ipcomp']},
        {name:'--mode',label:'Mode',type:'select',options:['tunnel','transport']},
        {name:'--tunnel-src',label:'Tunnel Src',type:'text'},
        {name:'--tunnel-dst',label:'Tunnel Dst',type:'text'},
        {name:'--next',label:'Next',type:'boolean'},
    ]},
    'psd' : {label:'psd',params:[
        {name:'--psd-weight-threshold',label:'Psd weight threshold',type:'text'},
        {name:'--psd-delay-threshold',label:'Psd delay threshold',type:'text'},
        {name:'--psd-lo-ports-weight',label:'Psd lo ports weight',type:'text'},
        {name:'--psd-hi-ports-weight',label:'Psd hi ports weight',type:'text'},
    ]},
    'quota' : {label:'quota',params:[
        {name:'--quota',label:'Quota',type:'text'},
    ]},
    'random' : {label:'random',params:[
        {name:'--average',label:'Average',type:'text'},
    ]},
    'realm' : {label:'realm',params:[
        {name:'--realm',label:'Realm',type:'text'},
    ]},
    'recent' : {label:'recent',params:[
        {name:'--name',label:'Name',type:'text'},
        {name:'--set',label:'Set',type:'boolean'},
        {name:'--rcheck',label:'Rcheck',type:'boolean'},
        {name:'--update',label:'Update',type:'boolean'},
        {name:'--remove',label:'Remove',type:'boolean'},
        {name:'--seconds',label:'Seconds',type:'text'},
        {name:'--hitcount',label:'Hitcount',type:'text'},
        {name:'--rttl',label:'Rttl',type:'boolean'},
    ]},
    'sctp' : {label:'sctp',params:[
        {name:'--source-port',label:'Source Port',type:'text'},
        {name:'--destination-port',label:'Destination Port',type:'text'},
        {name:'--chunk-types',label:'Chunk Types',type:'text'},
    ]},
    'set' : {label:'set',params:[
        {name:'--set',label:'Set',type:'text'},
    ]},
    'state' : {label:'state',params:[
        {name:'--state',label:'State',type:'select',options:['INVALID','ESTABLISHED','NEW','RELATED'], multiple:true},
    ]},
    'string' : {label:'string',params:[
        {name:'--algo',label:'Algo',type:'select',options:['bm','kmp']},
        {name:'--from',label:'From',type:'text'},
        {name:'--to',label:'To',type:'text'},
        {name:'--string',label:'String',type:'text'},
        {name:'--hex-string',label:'Hex String',type:'text'},
    ]},
    'tcpmss' : {label:'tcpmss',params:[
        {name:'--mss',label:'MSS',type:'text'},
    ]},
    'time' : {label:'time',params:[
        {name:'--timestart',label:'Time Start',type:'time'},
        {name:'--timestop',label:'Time Stop',type:'time'},
        {name:'--days',label:'Days',type:'select', options:['Mon','Tue','Wed','Thu','Fri','Sat','Sun']},
        {name:'--datestart',label:'Date Start',type:'date'},
        {name:'--datestop',label:'Date Stop',type:'date'},
    ]},
    'tos' : {label:'tos',params:[
        {name:'--tos',label:'TOS',type:'text'},
    ]},
    'ttl' : {label:'ttl',params:[
        {name:'--ttl-eq',label:'TTL EQ',type:'text'},
        {name:'--ttl-gt',label:'TTL GQ',type:'text'},
        {name:'--ttl-lt',label:'TTL LQ',type:'text'},
    ]},
    'u32' : {label:'u32',params:[]},
    'unclean' : {label:'unclean',params:[]},
};