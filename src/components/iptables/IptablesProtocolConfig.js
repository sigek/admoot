export default {
    tcp : {
        label:'TCP', 
        params : [
            {name:'--sport', label:'来源端口', type:'text'},
            {name:'--dport', label:'目标端口', type:'text'},
            {name:'--tcp-flags', label:'标志位', type:'text'},
            {name:'--tcp-option', label:'TCP Option', type:'text'},
            {name:'--mss', label:'MSS', type:'text'},
        ]
    },
    udp : {
        label:'UDP', 
        params : [
            {name:'--sport', label:'来源端口', type:'text'},
            {name:'--dport', label:'目标端口', type:'text'},
        ]
    },
};