export default {
    'BALANCE' : {label:'BALANCE', params : [{name:'--to-destination', label:'To Destination', type:'text'}]},
    'CLASSIFY' : {label:'CLASSIFY',params:[{name:'--set-class',label:'Set Class',type:'text'}]},
    'CLUSTERIP' : {label:'CLUSTERIP',params:[
        {name:'--new',label:'New',type:'text'}, 
        {name:'--hashmode',label:'Hash Mode',type:'select', options:['sourceip', 'sourceip-sourceport', 'sourceip-sourceport-destport']},
        {name:'--clustermac',label:'Cluster Mac', type:'text'},
        {name:'--total-nodes',label:'Total Nodes',type:'text'},
        {name:'--local-node',label:'Local Node',type:'text'},
        {name:'--hash-init',label:'Hash Init',type:'text'},
    ]},
    'CONNMARK' : {label:'CONNMARK', table : 'mangle', params :[
        {name:'--set-mark',label:'Set Mark', type:'text'},
        {name:'--save-mark',label:'Save Mark', type:'text'},
        {name:'--restore-mark',label:'Restore Mark', type:'text'},
    ]},
    'DNAT' : {label:'DNAT', table:'nat', chain:['PREROUTING','OUTPUT'], protocol:['tcp','udp'], params : [
        {name:'--to-destination', label:'To Destination', type:'text'},
    ]},
    'DSCP' : {label:'DSCP', table:'mangle', params : [
        {name:'--set-dscp', label:'DSCP', type:'text'},
        {name:'--set-dscp-class', label:'DSCP Class', type:'text'},
    ]},
    'ECN' : {label:'ECN', table:'mangle', protocol:['tcp'], params : [
        {name:'--ecn-tcp-remove', label:'ECN TCP Remove', type:'boolean'},
    ]},
    'IPMARK' : {label:'IPMARK', table:'mangle',chain:['PREROUTING','POSTROUTING','FORWARD'], params : [
        {name:'--addr', label:'To Destination', type:'text'},
        {name:'--and-mask', label:'And Mask', type:'text'},
        {name:'--or-mask', label:'Or Mask', type:'text'},
    ]},
    'IPV4OPTSSTRIP' : {label:'IPV4OPTSSTRIP', params : []},
    'LOG' : {label:'LOG', params : [
        {name:'--log-level', label:'Log Level', type:'select', options:['debug','info','notice','warning','error','crit','alert','emerg']},
        {name:'--log-prefix', label:'Log Prefix', type:'text'},
        {name:'--log-tcp-sequence', label:'Log Tcp Sequence', type:'boolean'},
        {name:'--log-tcp-options', label:'Log Tcp Options', type:'boolean'},
        {name:'--log-ip-options', label:'Log Ip Options', type:'boolean'},
        {name:'--log-uid', label:'Log Uid', type:'boolean'},
    ]},
    'MARK' : {label:'MARK', table:'mangle', params : [
        {name:'--set-mark', label:'Set Mark', type:'text'},
    ]},
    'MASQUERADE' : {label:'MASQUERADE', table:'nat',chain:['POSTROUTING'], protocol:['tcp','udp'], params : [
        {name:'--to-ports', label:'To Ports', type:'text'},
    ]},
    'MIRROR' : {label:'MIRROR', chain:['INPUT','FORWARD','PREROUTING'], params : []},
    'NETMAP' : {label:'NETMAP', table:'nat', params : [
        {name:'--to', label:'To', type:'text'},
    ]},
    'NFQUEUE' : {label:'NFQUEUE', params : [
        {name:'--queue-num', label:'Queue Num', type:'text'},
    ]},
    'NOTRACK' : {label:'NOTRACK', table : 'raw', params : []},
    'REDIRECT' : {label:'REDIRECT', table : 'nat', chain : ['PREROUTING','OUTPUT'], params : [
        {name:'--to-ports', label:'To Ports', type:'text'},
    ]},
    'REJECT' : {label:'REJECT', chain:['INPUT','FORWARD','OUTPUT'], params : [
        {name:'--reject-with', label:'Reject With', type:'select', options:[
            'icmp-net-unreachable',
            'icmp-host-unreachable',
            'icmp-port-unreachable',
            'icmp-proto-unreachable',
            'icmp-net-prohibited',
            'icmp-host-prohibited',
            'icmp-admin-prohibited',
        ]},
    ]},
    'SAME' : {label:'SAME', params : [
        {name:'--to', label:'To', type:'text'},
        {name:'--nodst', label:'Nodst', type:'boolean'},
    ]},
    'SET' : {label:'SET', params : [
        {name:'--add-set', label:'Add Set', type:'text'},
        {name:'--del-set', label:'Del Set', type:'text'},
    ]},
    'SNAT' : {label:'SNAT', table:'nat', chain:['POSTROUTING'], protocol:['tcp','udp'], params : [
        {name:'--to-source', label:'To Source', type:'text'},
    ]},
    'TARPIT' : {label:'TARPIT', params : []},
    'TCPMSS' : {label:'TCPMSS', table:'mangle', params : [
        {name:'--set-mss', label:'Set Mss', type:'text'},
        {name:'--clamp-mss-to-pmtu', label:'Clamp Mss To Pmtu', type:'text'}
    ]},
    'TOS' : {label:'TOS', table:'mangle', params : [
        {name:'--set-tos', label:'TOS', type:'text'},
    ]},
    'TRACE' : {label:'', params : []},
    'TTL' : {label:'TTL', params : [
        {name:'--ttl-set', label:'TTL Set', type:'text'},
        {name:'--ttl-dec', label:'TTL Dec', type:'text'},
        {name:'--ttl-inc', label:'TTL Inc', type:'text'},
    ]},
    'ULOG' : {label:'ULOG', params : [
        {name:'-ulog-nlgroup', label:'Ulog Nlgroup', type:'select',options:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32]},
        {name:'--ulog-prefix', label:'Ulog Prefix', type:'text', maxlength:32},
        {name:'--ulog-cprange', label:'Ulog Cprange', type:'text'},
        {name:'--ulog-qthreshold', label:'Ulog Qthreshold', type:'text'},
    ]},
    'XOR' : {label:'', params : [
        {name:'--key', label:'Key', type:'text'},
        {name:'--block-size', label:'Block Size', type:'text'},
    ]},
};