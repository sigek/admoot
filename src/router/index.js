import Vue from 'vue'
import VueRouter from 'vue-router'
import IptablesMain from '../components/iptables/Main.vue';
import CrontabMain from '../components/crontab/Main.vue';
import DashboardMain from '../components/dashboard/Main.vue'
import StorageAnalysisMain from '../components/storage-analysis/Main.vue'
import ProcessManagerMain from '../components/process-manager/Main.vue'
import NetworkStatusMain from '../components/network-status/Main.vue'
import ApacheVirtualHostList from '../components/apache/VirtualHostList.vue'
import MysqlDatabaseList from '../components/mysql/MysqlDatabaseList.vue'
import MysqlUserManagement from '../components/mysql/MysqlUserManagement.vue'
import MysqlServerConfig from '../components/mysql/MysqlServerConfig.vue'
import MysqlServerStatus from '../components/mysql/MysqlServerStatus.vue'
import FileManagement from '../components/filesystem/FileManagement.vue'
Vue.use(VueRouter)

const routes = [
  {path: '/dashboard',name:'dashboard',component:DashboardMain},
  {path: '/storage-analysis',name:'storage-analysis',component:StorageAnalysisMain},
  {path: '/process-manager',name:'process-manager',component:ProcessManagerMain},
  {path: '/network-status',name:'network-status',component:NetworkStatusMain},
  {path: '/iptables-setting',name: 'iptables-setting',component: IptablesMain},
  {path: '/crontab-setting',name:'crontab-setting',component:CrontabMain},
  {path: '/apache-virtualhost-list', name:'apache-virtualhost-list',component:ApacheVirtualHostList},
  {path: '/mysql-database-list',name:'mysql-database-list',component:MysqlDatabaseList},
  {path: '/mysql-user-list',name:'mysql-user-list',component:MysqlUserManagement},
  {path: '/mysql-server-config',name:'mysql-server-config',component:MysqlServerConfig},
  {path: '/mysql-server-status',name:'mysql-server-status',component:MysqlServerStatus},
  {path: '/file-system-file-management',name:'file-system-file-management',component:FileManagement},
]

const router = new VueRouter({routes})
export default router
